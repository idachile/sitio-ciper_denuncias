<?php
/*
  Plugin Name: BulletMail
  Plugin URI: http://ida.cl
  Description: Integración de WP con APi Mailgun para programar
  Version: 1.0
  Author: Ideas Digitales Aplicadas
  Author URI: http://ida.cl
  License: Open Source
 */

/**
 * Clase para integrar el api de MailGun a distintos tipos de soliciones y automatizaciones a nivel de PHP y Wordpress.
 * El objetivo es facilitar la ejecución de algunos requerimientos sencillos como envío de email a listas y adminsitración de usuarios suscritos a Wordpress
 * permitiendo a la vez realizar tanto tareas CLI y de procesamiento masivo como HTTP.
 */
use Mailgun\Mailgun;

class bullet {

    var $mgClient;
    var $domain;

//    var $listAddress;

    /**
     * funcion constructora que incluye las API key y el dominio
     * @TODO Sacar estos datos de constantes y sobreescribirlos desde WP y Base de datos (wp_options)
     * @TODO Agregar datos de Listas y otros configurables
     * @api Mailgun
     */
    public function __construct() {
        require 'vendor/autoload.php';
    }

    /**
     * Permite agregar un usuario a una lista definida en $this->listAddress
     *
     * @param array $miembro Ejempplo  array('address' => 'bar@example.com', 'name' => 'Bob Bar', 'subscribed' => true, 'vars' => '{"age": 26}' );
     * @api post/$lista/members, array
     * @return json Devuelve JSON con datos de éxito del API o Error con status y mensaje de error
     *
     */
    function agregar_miembro($miembro) {
        try {
            $result = $mgClient->post("lists/$this->listAddress/members", $miembro);
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al crear al usuario", "log" => $e));
        }
        return $result;
    }

    function agregar_usuarios_bbdd_json() {

    }

    /**
     * Permite agregar masivamente usuarios a la lista definida en $this->listAddress, tomando los usuarios desde un archivo csv. La estructura del CSV es
     * ID;Nombre y Apellido;EMAIL
     * El script toma los datos del csv y los transforma en un JSON para subirlos vía api a la lista
     *
     * El tope máximo de usuarios a subir es de 1000 (999 comenzando en 0) por bucle
     *
     * @param int $cuantos Valor máximo de 1000 (999 comenzando en 0)
     * @param string $file_abspath ruta absoluta del archivo CSV a parsear y que debe contener solo datos correctos, pudiendo ser la primera línea de cabeceras de  datos. El separador es ;
     * @api post/$lista/members.json, array
     * @return json Devuelve JSON con datos de éxito del API o Error con status y mensaje de error
     *
     */
    function agregar_usuarios_file_json($cuantos = 999, $file_abspath) {
        $lineas = file_get_contents($file_abspath);
        $lineas = explode("\n", $lineas);

        //elimina la primera de cabecera
        unset($lineas[0]);
        $i = 0;
        $usuarios = "";
        $result = array();
        foreach ($lineas as $linea) {
            list($ID, $nombre, $email) = explode(";", $linea);
            if ($i < $cuantos) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $user['name'] = $nombre;
                    $user['address'] = $email;
                    $user['vars'] = array("wp_id" => $ID);
                    $usuarios .= json_encode($user, JSON_FORCE_OBJECT) . ",";
                }
            }
            if ($i == ($cuantos - 1)) {
                //elimina la última coma para que el json no falle
                $usuarios = rtrim($usuarios, ",");
                try {
                    $result[] = $mgClient->post("lists/$this->listAddress/members.json", array('members' => '[' . $usuarios . ']', 'upsert' => true));
                } catch (Exception $e) {
                    $result[] = json_encode(array("error" => "err", "message" => "Hubo un error al crear al usuario", "log" => $e));
                }
                $usuarios = "";
                $i = 0;
            } else {
                $i++;
            }
        }
        return $result;
    }
    /**
     * Permite eliminar un usuario de una lista de email definida en $this->listAddress
     *
     * @param string $email
     * @api post/$lista/members.json, array
     * @return json Devuelve JSON con datos de éxito del API o Error con status y mensaje de error
     *
     */
    function eliminar_usuarios_by_mail($email) {
        try {
            $result = $this->mgClient->delete("lists/$this->listAddress/members/$email");
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al eliminar al usuario", "log" => $e));
        }
        return $result;
    }

    function crear_lista($list, $description) {

        try {
            $result = $this->mgClient->post("lists", array(
                'address'     => $list,
                'description' => $description
            ));
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al crear la lista", "log" => $e));
        }
        return $result;

    }

    function borrar_lista() {

    }


    /**
     *  @param data  array
     *
     *   'address'     => 'bar@example.com',
     *   'name'        => 'Bob Bar',
     *   'description' => 'Developer',
     *   'subscribed'  => true,
     *   'vars'        => '{"age": 26}'
     */
    function agregar_miembro_lista($data){
        try {
            $result = $this->mgClient->post("lists/$this->listAddress/members", $data);
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al agregar el usuario a la lista", "log" => $e));
        }
        return $result;
    }
    /**
     * Permite mandar el email en formato html
     *
     * @param string $email
     * @api post/$lista/members.json, array
     * @return json Devuelve JSON con datos de éxito del API o Error con status y mensaje de error
     *
     */
    function mandar_mail() {
        if (isset($this->from)) {
            $email_options['from'] = $this->from;
        }
        if (isset($this->to)) {
            $email_options['to'] = $this->to;
        }
        if (isset($this->cc)) {
            $email_options['cc'] = $this->cc;
        }
        if (isset($this->subject)) {
            $email_options['subject'] = $this->subject;
        }
        if (isset($this->html)) {
            $email_options['html'] = $this->html;
        }
        if (isset($this->text)) {
            $email_options['text'] = $this->text;
        }
        if (isset($this->tags)) {
            $email_options['o:tag'] = $this->tags;
        }
        if (isset($this->deliverytime)) {
            $email_options['o:deliverytime'] = $this->deliverytime;
        }
        if (isset($this->recipient_vars_json)) {
            //'{"bob@example.com": {"first":"Bob", "id":1}, "alice@example.com": {"first":"Alice", "id": 2}}'
            $email_options['recipient-variables'] = $this->recipient_vars_json;
        }
        try {
            $result = $this->mgClient->sendMessage($this->domain, $email_options);
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al enviar el mensaje", "log" => $e));
        }
        return $result;
    }

    function guardar_rebotes($next = false) {
        if ($next != false) {
            $next = "?$next";
        }
        $r = $this->mgClient->get("$this->domain/bounces$next", array("limit" => 1000));
        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $mails_validos = file_get_contents('mails_validos_mg.csv');
            $mails_validos .= $email->address . "," . "\n";
            file_put_contents('mails_validos_mg.csv', $mails_validos);
        }
        if ($r->http_response_body->paging->next) {
            $next = explode("?", $r->http_response_body->paging->next);
            echo urldecode($next[1]) . "\n";
            $this->guardar_rebotes(urldecode($next[1]));
        }
    }

    function guardar_aperturas($next = false) {
        if ($next != false) {
            $next = "/$next";
        }
        $queryString = array(
            'begin' => 'Mon, 21 Dec 2013 00:00:00 -0000',
            'ascending' => 'yes',
            'limit' => 300,
            'subject' => 'Mensaje Fin de año',
            'event' => 'failed'
        );

        $r = $this->mgClient->get("$this->domain/events$next", $queryString);


        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $mails_validos = file_get_contents('mails_fallidos.csv');
            echo $mails_validos .= $email->recipient . ";" . $email->reason . "\n";
            $this->eliminar_usuarios_by_mail($email->recipient);
            file_put_contents('mails_fallidos.csv', $mails_validos);
        }
        if ($r->http_response_body->paging->next) {
            $next = explode("events/", $r->http_response_body->paging->next);
//            echo urldecode($next[1]) . "\n";
            $this->guardar_aperturas(urldecode($next[1]));
        }
    }

    function eliminar_rebotes($next = false) {
        if ($next != false) {
            $next = "?$next";
        }
        $r = $this->mgClient->get("$this->domain/bounces" . $next);
        $result = array();
        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $result[] = $this->eliminar_usuarios_by_mail($email->address);
            echo $email->address . "\n";
        }
        if ($r->http_response_body->paging->next) {
            $next = explode("?", $r->http_response_body->paging->next);
            echo urldecode($next[1]) . "\n";
            $this->eliminar_rebotes(urldecode($next[1]));
        }

        return $result;
    }

    function eliminar_bajas() {
        $r = $this->mgClient->get("$this->domain/unsubscribes");
        $result = array();
        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $result[] = $this->eliminar_usuarios_by_mail($email->address);
        }
        return $result;
    }

    function eliminar_compilants() {
        $r = $this->mgClient->get("$this->domain/complaints");
        $result = array();
        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $result[] = $this->eliminar_usuarios_by_mail($email->address);
        }
        return $result;
    }

    function obtener_logs() {
        try {
            $result = $result = $this->mgClient->get("$this->domain/log", array(
                'limit' => 100,
                'skip' => 0
            ));
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al obtener los logs", "log" => $e));
        }
        return $result;
    }

    function guardar_miembros_lista($next = 0) {

        $r = $this->mgClient->get("lists/$this->listAddress/members", array('subscribed' => 'yes', 'limit' => 100, 'skip' => $next));

        $eliminar_mails = $r->http_response_body->items;
        foreach ($eliminar_mails as $email) {
            $mails_validos = file_get_contents('lista-grande.csv');
            $mails_validos .= $email->address . ";" . $email->vars->wp_id . ";" . $email->name . "; \n";
            file_put_contents('lista-grande.csv', $mails_validos);
        }
        $next +=100;
        $this->guardar_miembros_lista($next);
    }

    function validar_email($validateAddress) {
        try {
            $result = $this->pubKey->get("address/validate", array('address' => $validateAddress, 'api_key'=>$this->mgClient));
        } catch (Exception $e) {
            $result = json_encode(array("error" => "err", "message" => "Hubo un error al validar el email", "log" => $e));
        }
        return $result;
    }

}
////ida
$bulletIDA = new bullet();
if($key && $pubkey && $domain){
  $key = get_field("key", "options");
  $pubkey = get_field("pubkey", "options");
  $domain =  get_field("domain", "options");
  $bulletIDA->mgClient = new Mailgun($key);
  $bulletIDA->pubKey = new Mailgun($pubkey);
  $bulletIDA->domain = $domain;
}else{
  $bulletIDA->mgClient = new Mailgun('key-62c9d4c4e0f515b6e71325467c244126');
  $bulletIDA->pubKey = new Mailgun('pubkey-c236b361c45e55eab30d9c99489fc0dc');
  $bulletIDA->domain = 'mg.ida.cl';
}
