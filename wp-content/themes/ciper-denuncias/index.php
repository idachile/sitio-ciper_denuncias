<?php
get_header();

$_SESSION = array();
session_destroy();

?>
<section class="content  content--first">
    <div class="content__container">
        <h2 class="content__subtitle"><?php echo bloginfo('title'); ?></h2>
        <div class="gridle-row">
            <div class="gridle-gr-9 gridle-gr-12@tablet no-padding--vertical no-padding--left padd-small">

                <div class="content__excerpt">
                <?php echo apply_filters('the_content', get_field('bajada_portada', 'options')); ?>
                </div>
                <div class="denuncias-caller">
                <?php
                    if(current_user_can('editor')):
                        get_template_part('partials/indice/periodista');
                    else:
                        get_template_part('partials/indice/general');
                    endif;
                ?>
                </div>
            </div>
            <?php get_sidebar('front'); ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>
