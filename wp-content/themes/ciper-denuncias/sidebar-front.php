<aside class="gridle-gr-3 gridle-gr-12@tablet no-padding--vertical no-padding--right padd-small">
	<div class="common-box common-box--denuncia common-box--denuncia_min">
		<div class="common-box__body">
			<h4 class="common-box__title common-box__title--semi"><?php the_field('titulo_call_contacto', 'options'); ?></h4>
			<div class="common-box__excerpt">
				<?php echo apply_filters('the_content', get_field('bajada_call_contacto', 'options')); ?>
			</div>
		</div>
		<div class="common-box__button">
			<a href="<?php echo ensure_url(get_field('url_call_contacto', 'options')); ?>" target="_blank" class="button button--ghost button--full" title="Denunciar">
				<?php the_field('boton_call_contacto', 'options'); ?>
			</a>
		</div>
	</div>
</aside>