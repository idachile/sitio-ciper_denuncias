<?php
get_header();
the_post();
$cod = $_GET['cod'];
$wait = '';
if($cod == ''){
    $wait = ' waiting';
}
$envio_page = get_page_by_path('envio-denuncia', OBJECT, 'page');
$envio_page_url = get_permalink($envio_page);
$_SESSION = array();
session_destroy();
?>
<section class="content content--first">
    <div class="content__container">
        <h2 class="content__subtitle"><?php the_title(); ?></h2>
        <div class="gridle-row">
            <div class="gridle-gr-9 gridle-gr-12@tablet no-padding--vertical no-padding--left padd-small">
                <div class="content__excerpt">
                    <?php the_content(); ?>
                </div>

                <!-- wp json search -->
                <form action="/wp-json/buscar/denuncia/" method="post" class="search-form" data-module="common-form" data-validation="generic_ajax" autocomplete="off">
                    <input type="hidden" id="busqueda_page_id" name="busqueda_page_id" value="<?php echo $post->ID; ?>">
                    <div class="content__form">
                        <div class="form-control content__form-control">
                            <label for="search_key" class="form-control__label">Código de denuncia</label>
                            <input type="text" class="form-control__field" name="search_key" id="search_key" value="<?php echo $cod; ?>" placeholder="Ingresa tu código" data-role="cleanspace" required autocomplete="off">
                        </div>
                        <div class="form-control content__form-control">
                            <label for="user_key" class="form-control__label">Palabra clave</label>
                            <input type="password" class="form-control__field" name="user_key" id="user_key" placeholder="Ingresa tu palabra clave" data-custom-validation="matchPattern" data-minlength="8" data-minlength="16" data-pattern="[^\w\d]*(([0-9]+.*[A-Za-z]+.*)|[A-Za-z]+.*([0-9]+.*))" data-role="cleanspace" required autocomplete="off">
                        </div>
                    </div>
                    <div class="content__btn-holder">
                        <button class="content__btn <?php echo $wait; ?>" type="submit" id="busqueda-denuncia" name="buscar_denuncia">Buscar</button>
                        <a href="<?php echo $envio_page_url; ?>" class="content__link content__link--prev" title="Volver a envío de denuncia">Volver</a>
                    </div>
                </form>
                <div id="response"></div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>