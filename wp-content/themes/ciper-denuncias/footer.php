</main>
<footer class="the-footer">
    <div class="container gridle-no-gutter">
        <div class="the-footer__body">
        <?php
            $separate = '';
            $gdata = get_general_data();
        ?>
            <div class="the-footer__brand">
                <a href="<?php the_field('url_ciper','options'); ?>" title="Ir a Ciper Chile">
                <?php
                    echo $gdata['logo_footer_img'];
                ?>
                </a>
            </div>
            <ul class="the-footer__data">
            <?php
                if($gdata['direccion']):
                    echo '<li class="the-footer__data-item">';
                    echo    '<a href="'.$gdata['direccion_url'].'" title="Ver dirección de CIPER" target="_blank">'.$gdata['direccion'].'</a>';
                    echo '</li>';
                endif;

                if($gdata['telefono'] || $gdata['fax']):
                    if($gdata['telefono'] && $gdata['fax']):
                        $separate = ' | ';
                    endif;
                    echo '<li class="the-footer__data-item">';
                    if($gdata['telefono']):
                        echo 'Fono: <a href="tel:'.$gdata['telefono_url'].'" title="Llamar a CIPER" target="_blank">'.$gdata['telefono'].'</a>';
                    endif;
                    echo $separate;
                    if($gdata['fax']):
                        echo 'Fax: <a href="fax:'.$gdata['fax_url'].'" title="Enviar fax CIPER" target="_blank">'.$gdata['fax'].'</a>';
                    endif;
                    echo '</li>';
                endif;
            ?>
                <li class="the-footer__data-item">
                    Todos los Derechos Reservados. &copy;2017
                </li>
            </ul>
        </div>
    </div>
    <div class="subfooter">
         <div class="container">
             <div class="subfooter_container">
                 <figure class="subfooter__figure">
                     <img src="<?php echo get_template_directory_uri() ?>/dist/img/logos/open-society.png" alt="Logo open society" class="elastic-img">
                 </figure>
                 <div class="subfooter__body">
                     <p class="the-footer__data-item">El rediseño y desarollo de este nuevo sitio web fue posible gracias al financiamiento de la Open Society, que apoya al periodismo independiente</p>
                 </div>
             </div>
         </div>
     </div>
</footer>
<?php
wp_footer();
require_once('__meta-footer.php');
?>
