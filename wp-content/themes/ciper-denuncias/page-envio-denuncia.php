<?php
//create post this slug
$codigo_denuncia = get_seed_id(get_field('codigo_char', 'options'));

get_header();
the_post();

global $kb, $mb;

?>
<section class="content">
    <div class="content__container">
        <h2 class="content__subtitle"><?php the_title(); ?></h2>
        <div class="gridle-row">
            <div class="gridle-gr-9 gridle-gr-12@tablet no-padding--vertical no-padding--left padd-small">
                <div class="content__excerpt">
                    <?php the_content(); ?>
                </div>
                <div class="denuncias-codigo">
                    <h5 class="denuncias-codigo__title">Código:</h5>
                    <div class="denuncias-codigo__code">
                        <input type="text" data-target-name="cipercode" value="<?php echo $codigo_denuncia; ?>" disabled>
                        <button class="denuncias-codigo__copy" data-func="copyClipboard" data-target="cipercode">Copiar</button>
                        <span class="copyboard-status">Copiado</span>
                    </div>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>
<section class="bg-lightgray">
    <div class="content">
        <div class="content__container">
            <div class="gridle-row">
                <div class="gridle-gr-9 gridle-gr-12@tablet gridle-suffix-3 gridle-suffix-0@tablet no-padding--vertical no-padding--left padd-small">
                    <form id="denuncia_mensaje_enviar" class="content__holder" action="/wp-json/realizar/denuncia" method="post" data-module="common-form" data-validation="generic_submit" enctype="multipart/form-data" autocomplete="off">
                        <input type="hidden" name="codigo_denuncia" id="codigo_denuncia" value="<?php echo $codigo_denuncia ?>">
                        <div class="form-control" data-error-message="Utilice una contraseña con una longitud entre 8 y 16 caracteres, incluyendo al menos una letra y un número">
                            <label class="form-control__label content__mintitle" for="denuncia_mensaje">Ingrese su palabra secreta:</label>
                            <input type="password" name="user_key" id="user_key" class="form-control__field" placeholder="Ingrese su palabra secreta" data-role="cleanspace" data-custom-validation="matchPattern" data-minlength="8" data-minlength="16" data-pattern="[^\w\d]*(([0-9]+.*[A-Za-z]+.*)|[A-Za-z]+.*([0-9]+.*))" required>
                            <p class="form-control__text">Utilice una contraseña con una longitud entre 8 y 16 caracteres, incluyendo al menos una letra y un número</p>
                        </div>
                        <div class="form-control">
                            <label class="form-control__label content__mintitle" for="denuncia_mensaje">Ingrese su mensaje:</label>
                            <textarea name="denuncia_mensaje" id="denuncia_mensaje" rows="5" class="form-control__field" placeholder="Ingrese su mensaje" data-autoresize required></textarea>
                        </div>
                        <div class="form-control" data-error-message="El archivo pesa más de <?php echo $mb; ?>mb">
                            <div class="form-control--file">
                                <span class="content__btn--file">
                                    Seleccionar archivo
                                    <input type="file" class="full-file" name="denuncia_archivo" id="denuncia_archivo" data-role="validfile" data-max-size="<?php echo $kb; ?>" accept=".xlsx,.xls,image/*,audio/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                                </span>
                                <span class="content__btn--file__desc">Ningún archivo seleccionado</span>
                            </div>
                            <p class="form-control__text">Peso máximo de archivo <strong><?php echo $mb; ?>mb.</strong></p>
                        </div>
                        <div class="content__btn-holder content__btn-holder--spandex">
                            <button type="submit" class="content__btn waiting" name="denuncia_enviar">Enviar mensaje</button>
                            <a href="<?php echo home_url(); ?>" class="content__link content__link--prev" title="Volver al Inicio">Volver</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
