<div class="message message--ghost-danger" data-module="message">
	<div class="message__body">
	<?php
		$content_denuncia_error = get_field('busqueda_mensaje_error', $_POST['busqueda_page_id']);
		echo str_replace('%codigo_busqueda%', '<strong>' . $_POST['codigo_denuncia'] . '</strong>', $content_denuncia_error);
	?>
	</div>
	<button class="message__close-button" data-role="close"></button>
</div>
<script src="<?php echo get_bloginfo('template_directory'). '/assets/scripts/libs/jquery-3.1.0.min.js' ?>"></script>
<script src="<?php echo get_bloginfo('template_directory'). '/assets/scripts/src/messages.js' ?>"></script>