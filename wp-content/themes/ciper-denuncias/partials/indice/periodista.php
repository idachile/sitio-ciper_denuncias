<div class="denuncias-callbox denuncias-callbox--full">
	<div class="denuncias-callbox__header">
		<h4 class="denuncias-callbox__title">
			<?php the_field('titulo_call_periodista', 'options'); ?>
		</h4>
		<div class="denuncias-callbox__excerpt">
			<?php the_field('bajada_call_periodista', 'options'); ?>
		</div>
	</div>
	<div class="denuncias-callbox__extra">
		<a href="/busqueda-denuncia/" class="content__btn" title="title"><?php the_field('boton_call_periodista', 'options'); ?></a>
	</div>
</div>