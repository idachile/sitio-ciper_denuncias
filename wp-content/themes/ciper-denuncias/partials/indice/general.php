<div class="denuncias-callbox denuncias-callbox--gray">
	<h4 class="denuncias-callbox__title">
		<?php the_field('titulo_call_denuncia', 'options'); ?>
	</h4>
	<div class="denuncias-callbox__excerpt">
		<?php the_field('bajada_call_denuncia', 'options'); ?>
	</div>
	<div class="denuncias-callbox__extra">
		<a href="/envio-denuncia/" class="content__btn" title="Ir a <?php the_field('boton_call_denuncia', 'options'); ?>"><?php the_field('boton_call_denuncia', 'options'); ?></a>
	</div>
</div>
<div class="denuncias-callbox denuncias-callbox--clear">
	<h4 class="denuncias-callbox__title">
		<?php the_field('titulo_call_conversacion', 'options'); ?>
	</h4>
	<div class="denuncias-callbox__excerpt">
		<?php the_field('bajada_call_conversacion', 'options'); ?>
	</div>
	<div class="denuncias-callbox__extra">
		<a href="/busqueda-denuncia/" class="content__btn content__btn--ghost" title="Ir a <?php the_field('boton_call_conversacion', 'options'); ?>"><?php the_field('boton_call_conversacion', 'options'); ?></a>
	</div>
</div>
