<div class="content__pop-message content__pop-message--success">
    <h5 class="content__pop-message__title">
        <?php
            global $codigo_denuncia;
            $confirmacion_error_titulo = get_field('titulo_exito', $confirmacion_page_id);
            $confirmacion_error_titulo = str_replace('%codigo_denuncia%', '<strong>' . $codigo_denuncia . '</strong>', $confirmacion_error_titulo);
            echo $confirmacion_error_titulo;
        ?>
    </h5>
    <div class="content__pop-message__excerpt">
        <?php
            $confirmacion_error = get_field('mensaje_exito', $confirmacion_page_id);
            $confirmacion_error = str_replace('%codigo_denuncia%', '<strong>' . $codigo_denuncia . '</strong>', $confirmacion_error);
            echo apply_filters( 'the_content', $confirmacion_error );
        ?>
    </div>
</div>