<div class="content__pop-message content__pop-message--error">
	<h5 class="content__pop-message__title">
		<?php
			the_field('titulo_error', $confirmacion_page_id);
		?>
	</h5>
	<div class="content__pop-message__excerpt">
		<?php echo apply_filters( 'the_content', get_field('mensaje_error',$confirmacion_page_id) ); ?>
	</div>
</div>