<?php
    $email_contents = $GLOBALS['email_contents'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
		<?php echo $email_contents['title']; ?>
    </title>
    </head>

    <body bgcolor="#EEEEEE" style="overflow: hidden; box-sizing: border-box;">
        <main style="background: #FFFFFF; max-width: 600px; margin: 0 auto; width: 100%; ">

            <div class="header" style="width: 100%; padding: 0;  background-color: #092334; display: block; text-align: center;">
                <a href="<?php echo home_url(); ?>" title="Ir a Ciper Chile" target="_blank">
                    <img style="width:100px; height: auto; display: block; padding: 1rem; margin: 0 auto;" src="<?php echo get_bloginfo('template_directory'); ?>/dist/img/logos/logo-ciper-denuncia-light.png">
                </a>
            </div>

            <div class="content" style="width: 100%; max-width: 500px; margin: 0 auto; padding: 1rem; overflow: hidden; box-sizing: border-box;">
                <p><strong>Estimado/a:</strong> <br> <?php echo $email_contents['intro']; ?></p>
                
                <div style="margin: 2rem 0; max-width: 100%;">
                    <?php echo $email_contents['mensaje']; ?>
                </div>
                <div>
                    <p>Saludos,<br>
                    <strong>CIPER Chile</strong></p>

                    <p style="margin: .75rem 0 .25rem;"><i>Por favor no responder a este mail</i></p>
                    <div style="border-top: 1px solid #C7CDD1; margin-top: 1rem;">
                        <?php echo $email_contents['footer']; ?>
                    </div>
                </div>
            </div>

        </main>

    </body>

</html>
