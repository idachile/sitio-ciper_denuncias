<?php
global $post;
global $paged;

////////////
///BASICS///
////////////

//FUNC:
//Styles
function incrustrar_css() {
	  wp_register_style('main_style', get_bloginfo('template_directory').'/dist/css/main.css');
    wp_enqueue_style('main_style');
}
add_action('wp_print_styles', 'incrustrar_css');

//FUNC:
//Scripts
function incrustar_scripts(){
	wp_deregister_script('jquery');
	wp_enqueue_script('main', get_bloginfo('template_directory'). '/dist/js/main.min.js', NULL, array('jquery'), true);
}
add_action( 'wp_enqueue_scripts', 'incrustar_scripts');

//FUNC:
//Menu setup
add_action('after_setup_theme', 'menu_setup');
if (!function_exists('menu_setup')) {
    function menu_setup() {
        add_theme_support('post-formats', array('aside', 'video', 'image'));
        add_theme_support('custom-header');
        add_theme_support('automatic-feed-links');
        register_nav_menus(array(
            "Main" => "Este es el menu principal del tema"
        ));
    }
}
//FUNC:
//SVG mimetype allowed
add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
    // Add the file extension to the array
    $existing_mimes['svg'] = 'image/svg+xml';
    return $existing_mimes;
}

if( function_exists('acf_add_options_sub_page') ){
    // configuracion de generales
    acf_add_options_sub_page(array(
        'title' => 'Portada',
        'capability' => 'list_users'
    ));

    // configuracion de generales
    acf_add_options_sub_page(array(
        'title' => 'Generales',
        'capability' => 'list_users'
    ));
}

//WARNING
//Rename posts
function change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Denuncias';
    $submenu['edit.php'][5][0] = 'Denuncia';
    $submenu['edit.php'][10][0] = 'Add Denuncia';
    $submenu['edit.php'][16][0] = 'Denuncias Tags';
}
function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Denuncias';
    $labels->singular_name = 'Denuncia';
    // $labels->add_new = 'Add Denuncia';
    // $labels->add_new_item = 'Add new Denuncia';
    // $labels->edit_item = 'Edit Denuncia';
    $labels->new_item = 'New Denuncia';
    // $labels->view_item = 'View Denuncia';
    $labels->search_items = 'Search Denuncia';
    $labels->not_found = 'No Denuncias found';
    $labels->not_found_in_trash = 'No Denuncias found in Trash';
    $labels->all_items = 'All Denuncias';
    $labels->menu_name = 'Denuncias';
    $labels->name_admin_bar = 'Denuncias';
}
add_action( 'admin_menu', 'change_post_label' );
add_action( 'init', 'change_post_object' );

//hide admin elements for posts
function posttype_admin_css() {
    global $post_type;
    $post_types = array('post');
    if(in_array($post_type, $post_types))
    echo '<style type="text/css">#post-preview, #view-post-btn, #wp-admin-bar-view, #edit-slug-box, span.view, span a.editinline, a.page-title-action{display: none;}</style>';
}
add_action( 'admin_head-post-new.php', 'posttype_admin_css' );
add_action( 'admin_head-post.php', 'posttype_admin_css' );
add_action( 'pre_get_posts', 'posttype_admin_css' );

//remove bar for periodista
add_action('after_setup_theme', 'remove_editor_bar');
function remove_editor_bar() {
    if (current_user_can('editor') && !is_admin()) {
        show_admin_bar(false);
    }
}


//FUNC:
//Print <pre>
function printMe( $thing ){
    echo '<pre>';
    print_r( $thing );
    echo '</pre>';
}

//FUNC:
//Clean url
function ensure_url( $proto_url, $protocol = 'http' ){
    // se revisa si es un link interno primero
    if( substr($proto_url, 0, 1) === '/' ){
        return $proto_url;
    }if (filter_var($proto_url, FILTER_VALIDATE_URL)) {
        return $proto_url;
    }else if( substr($proto_url, 0, 7) !== 'http://' || substr($proto_url, 0, 7) !== 'https:/' ){
        $url = $protocol . '://' . $proto_url;
    }
    // doble chequeo de validacion de URL
    if ( ! filter_var($url, FILTER_VALIDATE_URL) ) {
        return '';
    }
    return $url;
}

//FUNC:
//Load template part, use inside echo
function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}

function upload_custom_file( $file_data, $mimes = null ){
    if ( ! function_exists( 'wp_handle_upload' ) ) { require_once( ABSPATH . 'wp-admin/includes/file.php' ); }
    $attach_id = 'null';
    if($file_data):
        $fotoUpload = wp_handle_upload( $file_data, array( 'mimes' => $mimes, 'test_form' => false ) );
        $filename = $fotoUpload['file'];
        $wp_filetype = wp_check_filetype(basename($filename), null );
        $wp_upload_dir = wp_upload_dir();
        $attachment = array(
            'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $filename ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $filename );
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );
    endif;

    return $attach_id;
}


function parse_mime_type($file_path) {
    $chunks = explode('/', $file_path);
    return substr(strrchr(array_pop($chunks), '.'), 1);
}

function get_type($fileid) {
    $file_path = get_attached_file($fileid);
    $mimetype = parse_mime_type($file_path);
    return $mimetype;
}

function get_attachment_file($id){
    $file = array();

    $args = array(
      'post_type' => 'attachment',
      'posts_per_page' => 1,
      'post_status' => 'inherit',
      'post__in' => array($id)
    );
    $attachment = get_posts( $args );

    if($attachment){
        $attachment = $attachment[0];

        $file['slug'] = $attachment->post_name . '.'. get_type($id);
        $file['title'] = $attachment->post_title;
        $file['url'] = wp_get_attachment_url($attachment->ID);
        $file['size'] = filesize(get_attached_file( $attachment->ID));

        if($file['title']&&$file['size']){
            $file = $file;
        }else{
            $file = array();
        }
    }

    return $file;
}

function get_full_date($pid){
    $date = get_post_time( 'd/m/Y', false, $pid);
    $time = get_post_time( 'H:i', false, $pid);

    return $date . ' a las ' . $time . ' hrs.';
}

function unset_full_date($dateacf){
    $datetime = DateTime::createFromFormat('d/m/Y H:i', $dateacf);
    $date = $datetime->format('d/m/Y');
    $time = $datetime->format('H:i');

    return $date . ' a las ' . $time . ' hrs.';
}

function gmap_url($address){
    $address = urlencode($address);
    $address = ensure_url('www.google.com/maps/place/' . $address);
    return $address;
}

function clean_string($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function current_datetime(){
    date_default_timezone_set('America/Santiago');
    return date('d/m/Y H:i');
}

function js_redirect($url){
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "' . $url . '"';
    $string .= '</script>';

    echo $string;
}

add_shortcode('secret', 'generate_secret_mensaje');
function generate_secret_mensaje($atts, $content){
    $settings = shortcode_atts(array(
        'key' => ''
    ), $atts);

    return ida_encrypt($content, $atts['key']);
}
////////////
///SPECS////
////////////

//FUNC:
//Get random metaid
function get_seed_id($length){
    $seed = array_flip(array_merge(range('a','z'),range(0,9)));
    $seed_id = implode("",array_rand($seed, $length));
    return $seed_id;
}

function get_general_data(){
    $data['direccion'] = get_field('direccion', 'options');
    $data['direccion_url'] = gmap_url($data['direccion']);
    $data['telefono'] = get_field('telefono', 'options');
    $data['telefono_url'] = 'tel:' . clean_string($data['telefono']);
    $data['fax'] = get_field('fax', 'options');
    $data['fax_url'] = 'fax:' . clean_string($data['fax']);
    $data['logo_footer_img'] = wp_get_attachment_image(get_field('logo_footer', 'options'), 'full', false, array('class' => 'the-footer__logo', 'alt'=>'Logo Ciper'));
    $data['logo_footer_url'] = wp_get_attachment_image_url(get_field('logo_footer', 'options'), 'full', false);
    $data['logo_dark_img'] = wp_get_attachment_image(get_field('logo_sidebar', 'options'), 'full', false, array('class' => 'elastic-img', 'alt'=>'Logo Ciper'));
    $data['logo_dark_url'] = wp_get_attachment_image_url(get_field('logo_sidebar', 'options'), 'full', false);
    $data['logo_light_img'] = wp_get_attachment_image(get_field('logo_sidebar_inverse', 'options'), 'full', false, array('class' => 'elastic-img', 'alt'=>'Logo Ciper'));
    $data['logo_light_url'] = wp_get_attachment_image_url(get_field('logo_sidebar_inverse', 'options'), 'full', false);

    return $data;
}

//FUNC:
//Get field conversacion.
function get_conversacion_denuncia($postid, $user_key){
    $conversacion_print = '';
    $conversacion = get_field('conversacion', $postid);
    if($conversacion):
        foreach($conversacion as $chat):
            $archivo = get_attachment_file($chat['archivo_adjunto']);
            $fecha = '<p class="content__chat__data">Enviado el '.unset_full_date($chat['fecha']).'</p>';
            $mainclass = $archivoprint = $pointer = '';

            if($archivo && $archivo !== 'null'):
                $archivoprint  = '<div class="content__chat__file">';
                $archivoprint .=    '<a href="'.$archivo['url'].'" title="Descargar archivo" download>'.$archivo['slug'].'</a>';
                $archivoprint .= '</div>';
            endif;

            if($chat['tipo_mensaje'] == 'respuesta'):
                $mainclass .= 'response';
                $remitente = $chat['remitente'];
                $extra = $fecha;
            else:
                $mainclass .= 'sent';
                $extra = $archivoprint . $fecha;
                current_user_can('editor') ? $remitente = 'Denunciante' : $remitente = 'Tu';;
            endif;

            if($chat == end($conversacion)) $pointer = 'id="last"';

            $mensaje_chat = ida_decrypt($chat['mensaje'], $user_key);

            $conversacion_print .= '<div '. $pointer .' class="content__chat__message content__chat__message--'.$mainclass.'" data-func="conversacion">';
            $conversacion_print .=    '<h4 class="content__chat__title">'.$remitente.':</h4>';
            $conversacion_print .=    '<div class="content__chat__excerpt">'. apply_filters('the_content', $mensaje_chat) .'</div>';
            $conversacion_print .=    $extra;
            $conversacion_print .= '</div>';
        endforeach;
    endif;

    // printMe(ida_decrypt('NTJOQVpNVEpjbHNEUHNFRFhUYm5CRGxRbGpVNU9xOWpjSndwaDJEOU1zOD0=', $user_key));

    return $conversacion_print;
}

function get_denuncia_by_code($code){
    $denuncias = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => 1,
        'post_status' => 'publish',
        'name' => $code
    ));

    return $denuncias;
}

function get_denuncia_by_key($data){
    $search_key = (string)$data['search_key'];
    $search_user = (string)$data['user_key'];
    $secret_key = ida_encrypt($search_key, $search_user);

    $denuncias = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => 1,
        'meta_key' => 'denuncia_key',
        'meta_value' => $secret_key
    ));

    return $denuncias;
}

function email_content($denuncia = '', $datetime = '', $user_key = ''){
    $gdata = get_general_data();

    $mensaje['body'] = '<div style="background-color:#F8F8F8; color: #092334; padding: 1.5rem;">';
    $mensaje['body'] .=     '<p style="font-style: italic; margin: 0 0 .25rem;">Código de denuncia: <a href="'.home_url().'/busqueda-denuncia?cod='.$denuncia.'" style="text-transform: uppercase; color: #092334; letter-spacing: .5px; font-weight:bold;" title="Ver denuncia">'.$denuncia.'</a></p>';
    $mensaje['body'] .=     '<p style="font-style: italic; margin: .25rem 0 0;">Utilice la siguiente palabra clave: <span style="font-weight: bold; letter-spacing: .5px;">'.$user_key.'</span></p>';
    $mensaje['body'] .=     '<br><p style="font-style: italic; margin: .25rem 0 0;">Fecha y hora de recepción: <span style="font-weight: bold; letter-spacing: .5px;">'. $datetime .' hrs.</span></p>';
    $mensaje['body'] .=     '<a href="'.home_url().'/busqueda-denuncia?cod='.$denuncia.'" title="Ver denuncia" style="display: inline-block; margin: 1rem 0 0; padding: .75rem 1rem; background: #1CBD99; color: #fff; font-weight: 500; text-decoration: none; border-radius: .25rem;">Ver denuncia</a>';
    $mensaje['body'] .= '</div>';

    $mensaje['footer'] = '<p style="line-height: 13px;font-size:12px;"><a href="'.$gdata['direccion_url'].'" title="Ver dirección en Google Maps" target="_blank" style="color: #A3ABB0;">'.$gdata['direccion'].'</a></p>';
    $mensaje['footer'] .= '<p style="line-height: 13px;font-size:12px;"><a href="'.$gdata['telefono_url'].'" title="Ver teléfono" target="_blank" style="color: #A3ABB0;">'.$gdata['telefono'].'</a></p>';

    return $mensaje;
}

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

function set_html_content_type(){
    return 'text/html';
}
function get_emails(){
		$email = 	get_field('emails_notificaciones', 'options');
    return $email[0];
}
function send_custom_email( $email_data, $return = false ){
    $type = $email_data['type'];
    $to = $email_data['to'];
    $subject = $email_data['subject'];
    $headers = $email_data['headers'];
    $attachments = isset($email_data['attachments']) && !empty($email_data['attachments']) ? $email_data['attachments'] : null;
    $GLOBALS['email_contents'] = $email_data['email_contents'];
    // se empieza un output buffer para contener el template del email
    ob_start();
    if($type == 'notificacion') get_template_part('partials/email/notificacion');
    $message = ob_get_clean();
    // temina el output buffer
    // solo en caso de que se quiera devolver el string del correo
    if( !!$return ){ return $message; }
    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
    wp_mail( $to, $subject, $message, $headers, $attachments );
    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    return true;
}

//FUNC:
//Enviar denuncia
function send_form_denuncia($data){
    $gdata = get_general_data();
    $wasitsent = $wasitsaved = false;
    // finfo para revisar los mime types
    $finfo = new finfo(FILEINFO_MIME_TYPE);

    if($_FILES['denuncia_archivo']['error'] === 0){
        $archivo_id = upload_custom_file( $_FILES['denuncia_archivo'] );
        if( !$archivo_id ){
            wp_die('Error al subir el archivo');
        }
    }

    // primero se hace el post
    // Primero se guarga el respaldo en un post tipo contacto.
    $new_id = wp_insert_post(array(
        'post_title' => 'Denuncia - ' . current_datetime(),
        'post_type' => 'post',
        'post_status' => 'publish'
    ));

    //encrypt sensitive info

    $denuncia_key = (string)$data['codigo_denuncia'];
    $denuncia_mensaje = (string)$data['denuncia_mensaje'];
    $user_key = (string)$data['user_key'];

    $encrypted_key = ida_encrypt($denuncia_key, $user_key);
    $encrypted_mensaje = ida_encrypt($denuncia_mensaje, $user_key);

    //Campo key alfanumérico
    update_field( "field_5b326327e657c", $encrypted_key, $new_id );
    // campo: "mensaje"
    update_field( "field_5a32760d25670", $encrypted_mensaje, $new_id );
    // campo: "archivo"
    update_field( "field_5a32763e25671", $archivo_id, $new_id );

    if( !$new_id || is_wp_error($new_id) ){
        wp_die('Error al crear inscripción');
        return 'invalid';
    }else{
        $wasitsaved = true;
    }

    $emails = get_emails();

    $mensaje = email_content($data['codigo_denuncia'], current_datetime(), $user_key);

    $wasitsent = send_custom_email(array(
        'type' => 'notificacion',
        'to' => '<'. $emails['notificacion'] .'>',
        'subject' => 'Nueva Denuncia Ciper',
        'headers' => array(
            'From: Ciper Chile <'. $emails['remitente'] .'>',
            'Reply-To: Ciper Chile <'. $emails['remitente'] .'>'
        ),
        'email_contents' => array(
            'title' => 'Nueva Denuncia Ciper',
            'intro' => 'Hemos recibido una nueva denuncia CIPER',
            'mensaje' => $mensaje['body'],
            'footer' => $mensaje['footer']
        )
    ));

    if($wasitsaved && $wasitsent) : return true; else: return 'invalid'; endif;

}

function send_form_comentario($data){
    $gdata = get_general_data();
    $wasitsent = $wasitupdate = false;

    //fecha actual
    $now = current_datetime();

    //datos de formulario
    $denuncia_id = $data['denuncia_id'];
    $denuncia = get_post($denuncia_id);
    //get conversación actual
    $conversacion = get_field('conversacion', $denuncia_id);

    $finfo = new finfo(FILEINFO_MIME_TYPE);

    if($_FILES['comentario_archivo']['error'] === 0){
        $archivo_id = upload_custom_file( $_FILES['comentario_archivo'] );
        if( !$archivo_id ){
            wp_die('Error al subir el archivo');
        }
    }else{
        $archivo_id = '';
    }

    $denuncia_mensaje = (string)$data['comentario_mensaje'];
    $user_key = (string)$data['user_key'];

    $encrypted_mensaje = ida_encrypt($denuncia_mensaje, $user_key);

    //Reset fecha para insertar en ACF field
    $reset_now = DateTime::createFromFormat('d/m/Y H:i', $now);
    $reset_now = $reset_now->format('Ymd H:i:s');

    //Set remitente
    $data['comentario_tipo'] !== 'pregunta' ? $remitente = 'Equipo Ciper' : $remitente = '';

    $insert_mensaje = array(
        'tipo_mensaje' => $data['comentario_tipo'],
        'remitente' => $remitente,
        'mensaje' => $encrypted_mensaje,
        'fecha' => $reset_now,
        'archivo_adjunto' => $archivo_id
    );
    
    if($conversacion):
        //actualizar (añadir a la última fila + 1)
        //contar cantidad de mensajes existentes
        count($conversacion) == 0 ? $count_row = 1 : $count_row = count($conversacion);
        $wasitupdate = update_row('field_5a33e6f7b104f', $count_row+1, $insert_mensaje, $denuncia_id);
    else:
        //añadir una fila
        $wasitupdate = add_row('field_5a33e6f7b104f', $insert_mensaje, $denuncia_id);
    endif;

    if($data['comentario_tipo'] !== 'pregunta') $ciper_responde = true;

    if(!$ciper_responde && $wasitupdate):
        $emails = get_emails();

        $mensaje = email_content($data['codigo_denuncia'], $now, $user_key);

        $wasitsent = send_custom_email(array(
            'type' => 'notificacion',
            'to' => '<'. $emails['notificacion'] .'>',
            'subject' => 'Nuevo comentario de Denuncia CIPER',
            'headers' => array(
                'From: Ciper Chile <'. $emails['remitente'] .'>',
                'Reply-To: Ciper Chile <'. $emails['remitente'] .'>'
            ),
            'email_contents' => array(
                'title' => 'Nuevo Comentario de Denuncia',
                'intro' => 'Hemos recibido un nuevo comentario de denuncia CIPER',
                'mensaje' => $mensaje['body'],
                'footer' => $mensaje['footer']
            )
        ));
    elseif($wasitupdate && $ciper_responde):
        $wasitsent = true;
    endif;

    if($wasitupdate && $wasitsent) : return true; else: return 'invalid'; endif;
}

/////////////////////////////////
///////////API REST//////////////
/////////////////////////////////

$api_rest = new api_rest();
class api_rest {
    function __construct() {
        add_action('rest_api_init', array($this, 'set_endpoints'));
    }

    function set_endpoints() {
        register_rest_route('buscar/', 'denuncia/', array(
            'methods' => 'POST',
            'callback' => array($this, 'buscar_denuncia')
        ));

        register_rest_route('realizar/', 'denuncia/', array(
            'methods' => 'POST',
            'callback' => array($this, 'realizar_denuncia')
        ));

        register_rest_route('enviar/', 'comentario/', array(
            'methods' => 'POST',
            'callback' => array($this, 'enviar_comentario')
        ));
    }

    function buscar_denuncia(WP_REST_Request $request){
        $data = $request->get_params();
        $codigo_denuncia = $data['search_key'];
        $user_key = $data['user_key'];

        $respuesta_page_url = get_permalink(get_page_by_path('respuesta-denuncia', OBJECT, 'page'));

        $denuncias = get_denuncia_by_key($data);

        $exito_response = $respuesta_page_url;
        $error_response = load_template_part('partials/busqueda-denuncia/error');

        session_start();
        if($denuncias->have_posts()):
            $_SESSION['codigo_denuncia'] = $codigo_denuncia;
            $_SESSION['user_key'] = $user_key;
            return array('response_html' => $exito_response, 'status' => 'ok');
        else:
            return array('response_html' => $error_response, 'status' => 'error');
        endif;
    }

    function realizar_denuncia(WP_REST_Request $request){
        $data = $request->get_params();

        $guardar_denuncia = send_form_denuncia($data);
        session_start();
        if($guardar_denuncia == true){
            $_SESSION['guardado'] = true;
            $_SESSION['status'] = 'exito';
            $_SESSION['codigo_denuncia'] = $data['codigo_denuncia'];
        }else{
            $_SESSION['status'] = 'error';
        }
        wp_redirect('/confirmacion-denuncia/');
        exit;
    }

    function enviar_comentario(WP_REST_Request $request){
        $data = $request->get_params();

        $guardar_comentario = send_form_comentario($data);

        session_start();
        if($guardar_comentario == true){
            $_SESSION['comentado'] = true;
        }else{
            $_SESSION['comentado'] = 'invalid';
        }

        wp_redirect('/respuesta-denuncia/');
        exit;
    }
}


//////////////////////////////////////////
//WATCH: ENCRYPT & DECRYPT via Password
//////////////////////////////////////////


function ida_encrypt($string, $secret_key) {
    // you may change these values to your own
    // $secret_key = 'ida_key';
    $secret_iv = 'ida_iv';
  
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
  
    return base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
  }
  
  function ida_decrypt($string, $secret_key) {
    // you may change these values to your own
    // $secret_key = 'ida_key';
    $secret_iv = 'ida_iv';
  
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
  
    $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    
    return $output;
 }




add_filter ('acf/save_post', 'on_save_denuncia_9', 9);
add_filter ('acf/save_post', 'on_save_denuncia_90', 90);
function on_save_denuncia_9($post_id){
    if( !empty($_POST['acf']) ){
        $cont = 0;
        foreach( $_POST['acf']['field_5a33e6f7b104f'] as $value ){
            $values[$cont]['original'] = $value['field_5a33eb626b9f7'];
            $new_value = preg_replace('#\\\\\\\\#','',stripslashes($value['field_5a33eb626b9f7']));
            if($values[$cont]['original'] != $new_value){
                $values[$cont]['new'] = apply_filters('the_content', $new_value);
            }else{
                $values[$cont]['new'] = $new_value;
            }
            $cont++;
        }
    }
    $counter = 0;
    foreach($values as $val){
        $_POST['acf']['field_5a33e6f7b104f'][$counter]['field_5a33eb626b9f7'] = $val['new'];
        $counter++;
    }
    
    return $post_id;
}

function on_save_denuncia_90($post_id){
    if( !empty($_POST['acf']) ){
        $cont = 0;
        foreach( $_POST['acf']['field_5a33e6f7b104f'] as $value ){
            $values[$cont]['original'] = $value['field_5a33eb626b9f7'];
            $new_value = preg_replace('#\\\\\\\\#','',stripslashes($value['field_5a33eb626b9f7']));
            if($values[$cont]['original'] != $new_value){
                $values[$cont]['new'] = apply_filters('the_content', $new_value);
            }else{
                $values[$cont]['new'] = $new_value;
            }
            $cont++;
        }
    }

    $counter = 0;
    foreach($values as $val){
        $_POST['acf']['field_5a33e6f7b104f'][$counter]['field_5a33eb626b9f7'] = $val['new'];
        $counter++;
    }
    
    return $post_id;
}
?>
