<?php
session_start();
$sending_form_valid = $_SESSION['comentado'];

get_header();
the_post();
global $mb, $kb;
$respuesta_page_url = get_permalink($post);
$respuesta_page_slug = $post->post_name;
$respuesta_page_id = $post->ID;

$codigo_denuncia = (string)$_SESSION['codigo_denuncia'];
$user_key = (string)$_SESSION['user_key'];


if($_SESSION['codigo_denuncia']){
    $codigo_denuncia = $_SESSION['codigo_denuncia'];
}else{
    js_redirect(home_url());
}
$find_data['search_key'] = $codigo_denuncia;
$find_data['user_key'] = $user_key;

$denuncia = get_denuncia_by_key($find_data);

if(current_user_can('editor')):
    $comentario_tipo = 'respuesta';
    $mensaje_tipo = 'respuesta';
    $denuncia_tag = 'Denunciante:';
else:
    $comentario_tipo = 'pregunta';
    $mensaje_tipo = 'mensaje';
    $denuncia_tag = 'Tu:';
endif;

?>
<section class="content content--first">
    <div class="content__container">
        <h2 class="content__subtitle"><?php the_title(); ?></h2>
        <div class="gridle-row">
            <div class="gridle-gr-9 gridle-gr-12@tablet no-padding--vertical no-padding--left padd-small">
                <div class="content__excerpt">
                    <?php the_content(); ?>
                </div>
                <div class="denuncias-codigo">
                    <h5 class="denuncias-codigo__title">Código:</h5>
                    <div class="denuncias-codigo__code">
                        <input type="text" data-target-name="cipercode" value="<?php echo $codigo_denuncia; ?>" disabled>
                        <button class="denuncias-codigo__copy" data-func="copyClipboard" data-target="cipercode">Copiar</button>
                        <span class="copyboard-status">Copiado</span>
                    </div>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>
<section class="bg-lightgray">
    <div class="content">
        <div class="content__container">
            <div class="gridle-row">
                <div class="gridle-gr-9 gridle-gr-12@tablet gridle-suffix-3 gridle-suffix-0@tablet no-padding--vertical no-padding--left padd-small">
                    <div class="content__chat">
                    <?php
                    //<START

                        while($denuncia->have_posts()):
                            $denuncia->the_post();
                            $pid = get_the_ID();
                            $encrypted_mensaje = get_field('denuncia_mensaje', $pid);
                            $mensaje = ida_decrypt($encrypted_mensaje, $user_key);
                            $archivo_id = get_field('denuncia_archivo', $pid);
                            $archivo = get_attachment_file($archivo_id);
                    ?>
                        <div class="content__chat__message content__chat__message--sent">
                            <h4 class="content__chat__title"><?php echo $denuncia_tag; ?></h4>
                            <div class="content__chat__excerpt">
                            <?php echo apply_filters('the_content', $mensaje ); ?>
                            </div>
                            <?php
                            if($archivo):
                            ?>
                            <div class="content__chat__file">
                                <a href="<?php echo $archivo['url']; ?>" title="Descargar archivo" download><?php echo $archivo['slug']; ?></a>
                            </div>
                            <?php
                            endif;
                            ?>
                            <p class="content__chat__data">Enviado el <?php echo get_full_date($pid); ?></p>
                        </div>
                    <?php
                        endwhile;
                        wp_reset_query();
                    //END>

                    //La conversación
                    echo get_conversacion_denuncia($pid, $user_key);
                    ?>
                    </div>
                <?php
                    if(!get_field('deshabilitar_comentarios', $pid)):
                ?>
                    <form class="content__holder" action="/wp-json/enviar/comentario" method="post" data-module="common-form" data-validation="generic_submit" enctype="multipart/form-data">
                        <input type="hidden" name="comentario_tipo" id="comentario_tipo" value="<?php echo $comentario_tipo; ?>">
                        <input type="hidden" name="denuncia_id" id="denuncia_id" value="<?php echo $pid; ?>">
                        <input type="hidden" name="user_key" id="user_key" value="<?php echo $user_key; ?>">
                        <input type="hidden" name="codigo_denuncia" id="codigo_denuncia" value="<?php echo $codigo_denuncia; ?>">
                        <input type="hidden" name="fecha" id="fecha" value="<?php echo date('d/m/Y H:i'); ?>">
                        <div class="form-control">
                            <label class="form-control__label content__mintitle" for="comentario_mensaje">Ingrese su <?php echo $mensaje_tipo; ?>:</label>
                            <textarea name="comentario_mensaje" id="comentario_mensaje" rows="5" class="form-control__field" placeholder="Ingrese su mensaje" data-autoresize required></textarea>
                        </div>
                        <?php
                            if($sending_form_valid):
                                get_template_part('partials/conversacion/ok');
                            elseif($sending_form_valid == 'invalid'):
                                get_template_part('partials/conversacion/error');
                            endif;
                        ?>
                        <div class="form-control" data-error-message="El archivo pesa más de <?php echo $mb; ?>mb">
                            <div class="form-control--file">
                                <span class="content__btn--file">
                                    Seleccionar archivo
                                    <input type="file" class="full-file" name="comentario_archivo" id="comentario_archivo" data-role="validfile" data-max-size="<?php echo $kb; ?>" accept=".xlsx,.xls,image/*,audio/*,.doc,.docx,.ppt,.pptx,.txt,.pdf">
                                </span>
                                <span class="content__btn--file__desc">Ningún archivo seleccionado</span>
                            </div>
                            <p class="form-control__text">Peso máximo de archivo <strong><?php echo $mb; ?>mb.</strong></p>
                        </div>
                        <div class="content__btn-holder content__btn-holder--spandex">
                            <button type="submit" class="content__btn waiting" name="comentario_enviar">Enviar <?php echo $mensaje_tipo; ?></button>
                            <?php wp_nonce_field('enviar_comentario', 'st_nonce'); ?>
                            <a href="<?php echo home_url(); ?>" class="content__link content__link--prev" title="Volver al Inicio">Volver a Home</a>
                        </div>
                    </form>
                <?php
                    endif;
                ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
