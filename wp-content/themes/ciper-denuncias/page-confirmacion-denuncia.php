<?php
get_header();
global $codigo_denuncia;
$status = $_SESSION['status'];
$codigo_denuncia = $_SESSION['codigo_denuncia'];
$_SESSION = array();
session_destroy();

if (!isset($status)){
    js_redirect(home_url());
}

the_post();

$confirmacion_page_id = $post->ID;
?>
<section class="content content--first">
    <div class="content__container">
        <h2 class="content__subtitle"><?php the_title(); ?></h2>
        <div class="gridle-row">
            <div class="gridle-gr-9 gridle-gr-12@tablet no-padding--vertical no-padding--left padd-small">
                <div class="content__excerpt">
                <?php get_template_part('partials/confirmacion-denuncia/'.$status); ?>
                </div>
                <div class="content__btn-holder content__btn-holder--end">
                    <a href="<?php the_field('url_ciper','options'); ?>" class="content__btn" title="Volver a Ciper">Volver a CIPER</a>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>
