$(document).ready(function () {
    $('.single__collapse__body').hide();
    $('.single__collapse__button').click(function () {
        $(this).siblings('.single__collapse__body').slideToggle();
        $(this).children('.icon-elem').toggleClass('animate');
    });
});