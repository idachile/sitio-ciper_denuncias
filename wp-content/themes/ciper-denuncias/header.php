<?php
require_once('__meta-header.php');
$menu = wp_get_nav_menu_items('main');
global $mb, $kb, $gdata;
$gdata = get_general_data();
$mb = get_field('peso_maximo', 'options');
$kb = $mb*1000000;
if(get_field('activar_menu', 'options')):
?>
<header class="header-bar">
    <nav class="nav-bar" data-module="nav-bar">
        <div class="container">
            <div class="nav-bar__holder">
                <div class="nav-bar__brand">
                    <a class="app-brand app-brand--inline" href="<?php echo home_url(); ?>">
                        <img class="app-brand__logo" src="<?php echo get_bloginfo('template_directory'); ?>/dist/img/logos/ciper_logo_light.svg" alt="Logo Ciper" />
                    </a>
                </div>
                <div class="nav-bar__body" data-role="nav-body">
                    <button class="nav-bar__deploy-button" aria-label="Ver menú" data-role="nav-deployer">
                        <span></span>
                    </button>
                    <div class="nav-bar__menu-holder">
                        <ul class="nav-bar__menu nav-bar__menu--primary">
                        <?php
                            foreach($menu as $menuitem):
                                echo '<li class="menu-item">';
                                echo    '<a href="'.$menuitem->url.'" title="Ver '.$menuitem->title.'">'.$menuitem->title.'</a>';
                                echo '</li>';
                            endforeach;
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
<?php
    endif;
?>
<main>