<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php bloginfo(name); ?></title>
	<meta name="description" content="<?php bloginfo('description');?>">
  <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico">  
	<?php session_start(); ?>
	<?php wp_head(); ?>
</head>
<body>
